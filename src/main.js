import Vue from 'vue'
import App from './App.vue'

import _Inicial from './Inicial.vue'
import _Fichas from './Fichas.vue'
import _Exemplos from './Exemplos.vue'
import _Rails_MySql from './Rails_MySql.vue'
import _DevOps_Assessment from './DevOps_Assessment.vue'
import _Pipeline from './Pipeline.vue'
import _Roadmap from './Roadmap.vue'

import VueRouter from 'vue-router'

Vue.use( VueRouter )

const router = new VueRouter(
  {
      routes: [
        {
          path: '/', component: _Inicial
        },
        {
          path: '/fichas', component: _Fichas
        },
        {
          path: '/exemplos', component: _Exemplos
        },
        {
          path: '/exemplos/rails_mysql', component: _Rails_MySql
        },
        {
          path: '/devops_assessment', component: _DevOps_Assessment
        },
        {
          path: '/pipeline', component: _Pipeline
        },
        {
          path: '/roadmap', component: _Roadmap
        }
      ]
  })

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
